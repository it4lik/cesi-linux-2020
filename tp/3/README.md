# TP3 : DNS, Backup et Docker

Ce TP a pour but d'étoffer l'infra montée dans le TP2 en y ajoutant quelques services d'infrastructure :
* DNS
* Sauvegarde
* appréhension de Docker avec un serveur Web conteneurisé

> Réutilisez les machines du TP2 comme support pour ce TP3.

<!-- vim-markdown-toc GitLab -->

* [I. DNS](#i-dns)
    * [1. Présentation](#1-présentation)
    * [2. Mise en place](#2-mise-en-place)
    * [3. Test](#3-test)
    * [4. Configuration du DNS de façon permanente](#4-configuration-du-dns-de-façon-permanente)
* [II. Backup](#ii-backup)
    * [1. Présentation](#1-présentation-1)
    * [2. Choix technique](#2-choix-technique)
    * [3. Mise en place](#3-mise-en-place)
* [III. Docker](#iii-docker)
    * [1. Présentation](#1-présentation-2)
    * [2. Mise en place](#2-mise-en-place-1)

<!-- vim-markdown-toc -->

# I. DNS

## 1. Présentation

> Créez une nouvelle machine qui portera ce service DNS.

Dans cette section, on va monter un serveur DNS afin de se passer du fichier `/etc/hosts` entre  les diffrentes machines du parc.

L'outil de référence est [bind](https://en.wikipedia.org/wiki/BIND), minimaliste mais très robuste et mature (c'est cette solution qui est derrière la plupart des DNS racines).

Nous allons pour cela avoir besoin d'écrire des [fichiers de zone](https://en.wikipedia.org/wiki/Zone_file). Ces fichiers standard décrivent à quel IP associer quel nom d'hôte au sein d'un domaine.

## 2. Mise en place

**NB : N'oubliez pas d'adapter avec vos adresses IP le contenu et le nom des fichiers de configuration.**

Ici, on considère les IPs et noms suivants :

| Machine        | IP            | Rôle                        |
|----------------|---------------|-----------------------------|
| `web.tp3.cesi` | `10.55.55.11` | Serveur Web                 |
| `db.tp3.cesi`  | `10.55.55.12` | Serveur de bases de données |
| `rp.tp3.cesi`  | `10.55.55.13` | Reverse proxy               |
| `ns1.tp3.cesi`  | `10.55.55.14` | Serveur DNS                 |

Installer le paquet `epel-release` si ce n'est pas déjà fait. Puis les paquets `bind` et `bind-utils`.

Il sera nécessaire de configurer trois fichiers :
* [/etc/named.conf](./dns/etc/named.conf)
* [/var/named/tp3.cesi.db](./dns/var/named/tp3.cesi.db)
* [/var/named/55.55.10.db](./dns/var/named/55.55.10.db)

Le firewall du serveur DNS devra accepter le trafic des ports `53/tcp` et `53/udp`.

L'unité de service de bind à démarrer avec `systemctl` s'appelle `named`.

## 3. Test

Pour tester, depuis une machine avec le paquet `bind-utils` d'installé :
```bash
# Avec les commandes dig, on peut spécifier à quel serveur DNS on veut poser une question avec le caractère @

# Résolution DNS
$ dig web.tp3.cesi @10.55.55.14

# Résolution inverse
$ dig -x 10.55.55.13 @10.55.55.14
```

## 4. Configuration du DNS de façon permanente

Il est possible de configurer l'utilisation de ce DNS de façon permanente en ajoutant la mention suivante au fichier `/etc/resolv.conf` :
```
nameserver 10.55.55.14
```

# II. Backup

## 1. Présentation

Le but de cette partie va être de mettre en place une sauvegarde du serveur Web. 

Pour ce faire, vous mettrez en place un script de backup, ainsi qu'un service `backup.service` qui lancera le script de façon périodique (on pourra aussi le déclencher manuellement avec un `systemctl start backup`).

Le but :
* écrire un script de backup `/opt/backup.sh`
  * les sauvegardes doivent être stockée dans `/opt/backup`
  * les sauvegardes doivent être compressées
  * il faut gérer le fait de ne garder que les 7 sauvegardes les plus récentes
* écrire une unité de service `backup.service`
  * elle lance le script `backup.sh`
  * inspirez-vous du service créé dans le TP1
* écrire un un fichier `backup.timer` (il permettra de lancer le service `backup.service` de façon périodique)

## 2. Choix technique

Pour l'outil de sauvegarde, je vous propose deux possibilités ici :
* utiliser `rsync`
  * utilitaire en ligne de commande
  * synchronise deux dossiers
  * outil très éprouvé
  * beaucoup de documentations/articles, c'est un cas d'école
* utiliser [borgbackup](https://www.borgbackup.org/)
  * utilitaire en ligne de commande
  * fonctionnalités avancées de sauvegarde :
    * chiffrement des sauvegardes
    * déduplication des données
    * compression
  * très puissant et simple d'utilisation

## 3. Mise en place

La structure du script `/opt/backup.sh` peut ressembler à :
```bash
#!/bin/bash

dir_to_backup='/var/www/'
destination='/opt/backup'

backup () {
  # Lance la sauvegarde de $dir_to_backup vers $destination
  # Commande rsync ou borg
}

rotate () {
  # Garde uniquement les 7 sauvegardes les plus récentes
}

backup
rotate
```

> **Pour la syntaxe, vous pouvez vous inspirer du [script vu en cours](./script/example.sh).**

Le fichier `/etc/systemd/system/backup.timer` :
```
[Unit]
Description=Run backup.service periodically

[Timer]
# Everyday at 04:00
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=multi-user.target
```

Il vous faudra rédiger un fichier `/etc/systemd/system/backup.service` en vous inspirant du `.service` du TP1. Ce fichier devra lancer le script `/opt/backup.sh`.

> Pour tester le bon fonctionnement du `backup.service`, vous pouvez le lancer avec `sudo systemctl start backup`.

Pour que le timer fonctionne :
* il faut qu'un fichier `backup.service` existe
* il faut désactiver le service
* il faut activer et démarrer le timer
* c'est à dire :
```bash
# Les deux fichiers qui portent le même nom, et un suffixe différent
$ ls /etc/systemd/system | grep backup
backup.service backup.timer

# On stoppe le démarage automatique de la backup s'il avait été activé
$ sudo systemctl disable backup.service

# On active l'exécution périodique de la backup via le timer
$ sudo systemctl enable backup.timer
$ sudo systemctl start backup.timer

# Etat du timer : on peut voir la date et heure de la dernière exécution, et de la prochaine
$ sudo systemctl list-timers
```

# III. Docker

## 1. Présentation

A la demande de quelques-uns, une brève partie sur Docker.

L'idée de cette partie va être de remplacer le Wordpress + Apache du serveur Web, par un service Wordpress conteneurisé avec Docker. 

Pour ce faire, la marche à suivre générale :
* stopper (voire désinstaller) le serveur web Apache de la machine web ainsi que Wordpress
* installer Docker sur la machine
* lancer un conteneur qui contient Wordpress

> D'un point de vue réseau, on aura toujours notre service qui écoute sur le port 80, conteneurisé ou non. Donc aucune modification du reverse proxy ne sera nécessaire pour qu'il continue à permettre de joindre l'application Web.

## 2. Mise en place

> Il est préférable d'aller détruire la base de données de Wordpress si vous aviez finalisé l'installation, pour éviter les conflits si Wordpress n'est pas dans une version identique par exemple.

A réaliser sur le serveur Web.

Couper (et désinstaller ?) Apache et Wordpress.

Installer Docker en suivant la [documentation officielle](https://docs.docker.com/engine/install/centos/).

Lancer un conteneur Wordpress :
```bash
# C'est une seule commande, juste c'est plus lisible de l'éclater sur plusieurs lignes avec des \ en fin de ligne :)
# N'oubliez pas de modifier avec vos infos de connexion à la base
docker run --name cesi-wp -d \
  -e WORDPRESS_DB_HOST=10.55.55.12 \
  -e WORDPRESS_DB_USER=cesi \
  -e WORDPRESS_DB_PASSWORD=cesi \
  wordpress
```

Tester le bon fonctionnement en visitant l'interface de Wordpress.
