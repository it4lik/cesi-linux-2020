# systemd

systemd est un outil qui est présent au coeur de la plupart des distributions GNU/Linux majeures :
* bases Debian (Debian, Ubuntu, Mint, etc.)
* bases RedHat (RHEL, CentOS, Fedora, etc.)
* bases Arch (ArchLinux, Manjaro, etc.)
* d'autres encore

Il occupe deux principales fonctions au sein du système :
* PID 1
* gestionnaire de services

# I. Rôles de systemd

## 1. PID 1

C'est **le premier processus lancé** par le kernel lors du boot de la machine.

C'est ensuite lui qui va lancer tous les autres processus, de façon ordonnée. 

Il s'occupe aussi de récupérer les processus orphelins : les processus dont le père aurait été tué de façon abrupte (crash, récupération de mémoire par le système, `kill` par l'administrateur, etc.).

## 2. Gestionnaire de services

> systemd est très inspiré de son équivalent du monde MacOS : launchd

systemd a aussi pour charge de **gérer les différents services présents sur la machine**. Par "service" on entend des applications dont il faut surveiller le cycle de vie. 

Ainsi, sur un Linux de bureau, c'est systemd qui se chargera d'entretenir le démon bluetooth ou encore le démon WiFi et la session graphique.  
Sur un serveur, c'est lui se chargera de lancer et surveiller tous les services comme serveurs Web, bases de données, etc.

Pour qu'il puisse gérer un service, il faut définir un *fichier d'unité*. C'est un simple fichier texte, avec une syntaxe particulière.  
Un exemple minimaliste pourrait ressembler à :

```
[Unit]
Description=Minimalist sample service

[Service]
Type=simple
ExecStart=/usr/bin/sleep 9999

[Install]
WantedBy=multi-user.target
```

> Si l'administrateur veut ajouter des unités au système, les fichiers sont à créer dans le répertoire `/etc/systemd/system/`.

## 3. Interaction avec les services

Interactions basiques avec un service :
```bash
sudo systemctl start <SERVICE>
sudo systemctl restart <SERVICE>
sudo systemctl stop <SERVICE>
```

Activer un service au démarrage :
```bash
sudo systemctl enable <SERVICE>
```

Afficher le fichier d'unité d'un service :
```bash
sudo systemctl cat <SERVICE>
```

Afficher le statut d'un service :
```bash
sudo systemctl status <SERVICE>
sudo systemctl is-enabled <SERVICE>
sudo systemctl is-active <SERVICE>
```

Afficher les logs d'un service :
```bash
sudo journalctl -xe -u <SERVICE>
```
